import { useState } from "react";
import { FaRegTrashCan, FaPencil, FaCheck, FaFloppyDisk } from "react-icons/fa6";
import custom_axios from "../axios/AxiosSetup";
import { ApiConstants } from "../api/ApiConstants";

interface ActiveTodoListProps {
  id: number;
  todo: string;
  dateTime: string;
  markCompelte: (id: number) => void;
  deleteTodo: (id: number) => void;
  changeTodo: (id: number, newTodo: string) => void;
}

const ActiveTodoList = (props: ActiveTodoListProps) => {
  const [editableTodo, setEditableTodo] = useState(props.todo);
  const [isEditing, setIsEditing] = useState(false);
  const [buttonClicked, setButtonClicked] = useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditableTodo(e.target.value);
  };

  const handleToggleEdit = () => {
    if (isEditing) {
      // Save changes
      custom_axios.patch(ApiConstants.TODO.CHANGE_NAME(props.id), { title: editableTodo }, { headers: { Authorization: "Bearer " + localStorage.getItem("token") } });
      props.changeTodo(props.id, editableTodo);
    }
    setIsEditing(!isEditing);

   // Toggle buttonClicked state
   setButtonClicked(!buttonClicked);
  };
  return (
    <li className="border-gray-400  flex flex-row">
      <div className="select-none bg-white flex flex-1 items-center p-4 transition duration-500 ease-in-out transform hover:-translate-y-2 rounded-2xl border-2 p-6 hover:shadow-2xl border-red-400">
        <div className="flex-1 pl-1 mr-16">
        {isEditing ? (
            <div>
              <input
                type="text"
                value={editableTodo}
                onChange={handleChange}
              />
            </div>
          ) : (
            <div>
              <span>{props.todo}</span>
            </div>
          )}
          <div className="font-medium">{props.dateTime}</div>
        </div>
        <button onClick={() => props.markCompelte(props.id)} className={`${
            isEditing ? "bg-gray-500 text-gray-700" : "bg-blue-500 hover:bg-blue-700 text-white"
          } font-bold py-2 px-4 rounded-full`}
          disabled={isEditing}>
          <FaCheck />
        </button>
        <button onClick={() => props.deleteTodo(props.id)} className={`${
            isEditing ? "bg-gray-500 text-gray-700" : "bg-red-500 hover:bg-red-700 text-white"
          } ml-2 font-bold py-2 px-4 rounded-full`}
          disabled={isEditing}>
          <FaRegTrashCan />
        </button>
        <button onClick={handleToggleEdit} className="ml-2 text-white font-bold py-2 px-4 rounded-full bg-yellow-500 hover:bg-yellow-700">
          {isEditing ? <FaFloppyDisk />: <FaPencil />}
        </button>
      </div>
    </li>
  );
};

export default ActiveTodoList;